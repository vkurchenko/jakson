import org.codehaus.jackson.map.ObjectMapper;

import java.io.FileInputStream;
import java.util.List;

public class Main {

    public static void main(String[] args) throws Exception {
        System.out.println("Hello World!");
        ObjectMapper mapper = new ObjectMapper();
        Example user = mapper.readValue(new FileInputStream("src/name.json"), Example.class);
        System.out.println(user);
    }
}

class Datum {

    private From from;
    private String message;

    public From getFrom() {
        return from;
    }

    public void setFrom(From from) {
        this.from = from;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}

class Example {

    private List<Datum> data = null;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
}

class From {

    private String name;
    private String age;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }
}

